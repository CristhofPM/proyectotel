# ProyectoTel


## Getting started
## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/CristhofPM/proyectotel.git
git branch -M main
git push -uf origin main
```

## Name
Sistema Inventario

## Installation
1.- Ejecutar archivo bd_prueba.sql en su base de datos MySql para la creación de la Base de Datos.
2.- Descargar el proyecto en su ruta de ejecución de proyectos PHP de manera local.
    Por ejemplo: 
        C:\AppServ\www\Nuevo_Proyecto
3.- Abrir su editor de codigo.
4.- Modificar los parametros de conexión a la base de datos en el directorio /sistema_prueba/modelo/conexion.php
    $conexion = new mysqli("localhost","root","verde21000","bd_prueba");

    "localhost" = Dirección.
    "root" = Cambiar por el nombre de usuario configurado en su base de datos.
    "verde21000" = Cambiar por la contraseña de su usuario de base de datos.
    "bd_prueba" = Nombre de la base de datos anteriormente creada.

5.- Iniciar el proyecto en la ruta descargada, el archivo principal es login.php
    Por ejemplo: 
        http://localhost/proyectotel/Practica/sistema_prueba/vistas/login.php


3.- Iniciar

## Usage
1.- Interfaz Login: Presenta un formulario para iniciar sesion en el aplicativo.
2.- Interfaz Inicio: Contiene una tabla de visualización de los equipos registrados.
3.- Interfaz Registro Equipo: Presenta un formulario de registro de equipo.

## Authors and acknowledgment
Realizado por: Cristhofer Peralta

## Project status
Completo:
    - Login Usuario, Inicio, Registro Usuario.

Por Realizar:
    - Registrar Usuario, Editar Equipo, Borrar Equipo
