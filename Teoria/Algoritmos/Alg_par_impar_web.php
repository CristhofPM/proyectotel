
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Par/Impar</title>
</head>
<body>
    <!--Formulario-->
    <form action="" method="POST">
        <label for="">Ingrese un número:</label>
        <input type="number" name="numero">

        <input type="submit" value="Par/Impar">
    </form>
    
<?php
    //Si recibe un método Post
    if($_POST)
    {
        //Asignar valor a la variable num con el valor que trae en el POST.
        $num = $_POST['numero'];

        //Condicion
        if(($num % 2)== 0){

            //Si el resultado es 0, imprime Par
            echo "<label> El numero es par </label>";
        }else{
            //Caso contrario, imprime Impar
            echo "<label> El numero es impar </label>";
        }
    }   
?>

</body>
</html>