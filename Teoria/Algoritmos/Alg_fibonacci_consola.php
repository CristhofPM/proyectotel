<?php

//Serie Fibonacci

//Variables
$valor1 = 0;
$valor2 = 1;
$serie_fibo = 0;
$num_ingresado = 0;

echo "Ingresa un numero: ";

//Guardar valor de consola en la variable num_ingresado
fscanf(STDIN, "%d", $num_ingresado);

//Imprime los valores iniciales
//echo $valor1 . "\n" . $valor2 . "\n";

//Recorrido
for ($i = 0; $i <= $num_ingresado; $i++){

    $serie_fibo = $valor1 + $valor2;
    $valor1 = $valor2;
    $valor2 = $serie_fibo;

    //secho $serie_fibo . "\n";

    //Validacion Numero Fibonacci
    if ($num_ingresado == $serie_fibo) {
        echo "El numero ingresado: " . $num_ingresado .  " - SI es Fibonacci";
        exit;
    }
}

echo "El numero ingresado: " . $num_ingresado .  " - NO es Fibonacci";