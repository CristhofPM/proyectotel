<?php

//Calcular si un numero es primo

$num_ingresado = 0;

//Ingreso de numero por teclado
echo "Ingrese un numero: ";
//Almacenamiento del valor en una variable
fscanf(STDIN, "%d", $num_ingresado);

//Funcion para determinar si un numero es primo o no
function NumPrimo($numero)
{
 for($i=2; $i<$numero; $i++)
   {
      if($numero %$i ==0)
	      {
		   return 0;
		  }
    }
  return 1;
   }

//Llamado a la funcion y almacenado en una variable
$resFuncion = NumPrimo($num_ingresado);

//Validacion del resultado de la funcion
if ($resFuncion==0)
echo 'El numero ingresado: '. $num_ingresado .' - No es un numero primo'."\n";
else
echo 'El numero ingresado: '. $num_ingresado .' - Si es un numero primo'."\n";