<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>

    <!--Bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  
</head>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    
    <div class="container-fluid">
        <div class="row">
        <div class="col-12 col-md-6 offset-md-3 p-5">
          <div class="card">
            <div class="card-body">
              <center><h1 class="p-2">Login</h1></center>
              <hr>

              <form method="POST" action="">

                <center>

                <?php
                
                include "../modelo/conexion.php";
                include "../controlador/controlador_login.php";

                ?>

                <label for="usuario">Usuario:</label>
                <br>
                <input type="text" name="usuario">
                <br>
                <label for="password">Contraseña:</label>
                <br>
                <input type="password" name="password">
                <br>             

                <input type="submit" class="btn btn-info" name="btnIngresar" value="Ingresar">
                </center>           

              </form>

            </div>
          </div>
        </div>   
        </div>
    </div>



  </body>
</html>