<?php
session_start();

if (empty($_SESSION["id_user"])){
    header("Location: ../login.php");
}

#Conexion
include("../../modelo/conexion.php");

$sql = "select * from equipos ";
$result = mysqli_query($conexion, $sql);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS v5.0.2 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Inicio</title>
</head>

<body>
    <!--NavBar-->
    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <div class="nav navbar-nav container-fluid">

            <a class="nav-item nav-link" href="#">
                Bienvenido,

                <?php
                echo $_SESSION["username"];
                ?>

                <span class="visually-hidden">(current)</span>
            </a>
            <a class="btn btn-outline-light" href="../../controlador/controlador_cerrar_sesion.php">Cerrar Sesión</a>
        </div>
    </nav>

    <!--Tabla-->
    <center class="p-2">
        <h1>TechGamer Inventory</h1>
    </center>

    <div class="card">
        <div class="card-header">

            <a name="" id="" class="btn btn-success" href="registrar.php" role="button">
                Registrar
            </a>

        </div>
        <div class="card-body">

            <table id="dtequipos" class="table table-bordered table-inverse table-responsive">
                <thead class="thead-inverse">
                    <tr>
                        <th>Local</th>
                        <th>Equipo</th>
                        <th>Cantidad</th>
                        <th>Fecha Actualización</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="dtbodyequipos">

                    <?php
                        while($datos=mysqli_fetch_array($result)){
                    ?>

                    <tr>
                        <td><?php echo $datos["local"] ?></td>
                        <td><?php echo $datos["descripcion"] ?></td>
                        <td><?php echo $datos["cantidad"] ?></td>
                        <td><?php echo $datos["fecha_registro"] ?></td>

                        <td class="row">
                            <a class="btn btn-warning btn-sm col-12" href="editar.php?id_equipo=<?php echo $datos["id_equipo"] ?>" role="button">
                                Editar
                            </a>
                            <a class="btn btn-danger btn-sm col-12" href="eliminar.php?id_equipo=<?php echo $datos["id_equipo"] ?>" role="button">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                    <?php
                         }
                    ?>
                </tbody>
            </table>

        </div>
        <div class="card-footer text-muted">
            <!--Footer tabla-->
        </div>
    </div>


</body>

</html>