
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar Equipo</title>

    <!-- Bootstrap CSS v5.0.2 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
    
<center><h1 class="p-2">Registrar Equipo</h1></center>

<div class="card">
    <div class="card-header">
    </div>
    <div class="card-body">
        <h4 class="card-title"></h4>

        <form action="../../controlador/controlador_registrar_usuario.php" method="post">

        <div class="mb-3">
            <label for="" class="form-label">
                Local:
            </label>
            <input type="text"
            class="form-control"
            name="local"
            aria-describedby="helpId" 
            placeholder=""
            value="">
            <br>
            <label for="" class="form-label">
              Equipo:
            </label>
            <input type="text"
            class="form-control"
            name="descripcion"
            aria-describedby="helpId" 
            placeholder=""
            value="">
            <br>
            <label for="" class="form-label">
              Cantidad:
            </label>
            <input type="number"
            class="form-control"
            name="cantidad"
            aria-describedby="helpId" 
            placeholder=""
            value="">
        </div>
        <center>
            <input name="btnRegistrar"class="btn btn-success" type="submit" value="Guardar">
            <a name="" id="" class="btn btn-danger" href="inicio.php" role="button">Cancelar</a>
        </center>
        </form>

    </div>
    <div class="card-footer text-muted">    
    </div>
</div>


</body>
</html>